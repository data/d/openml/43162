# OpenML dataset: lizards_dataset

https://www.openml.org/d/43162

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Dataset description**

Real-world data set about the perching behaviour of two species of lizards in the South Bimini island, from Shoener (1968).

**Format of the dataset**

The lizards data set contains the following variables:

    Species (the species of the lizard): a two-level factor with levels Sagrei and Distichus.

    Height (perch height): a two-level factor with levels high (greater than 4.75 feet) and low (lesser or equal to 4.75 feet).

    Diameter (perch diameter): a two-level factor with levels narrow (greater than 4 inches) and wide (lesser or equal to 4 inches).

**Source**

Edwards DI (2000). Introduction to Graphical Modelling. Springer, 2nd edition.

Fienberg SE (1980). The Analysis of Cross-Classified Categorical Data. Springer, 2nd edition.

Schoener TW (1968). "The Anolis Lizards of Bimini: Resource Partitioning in a Complex Fauna". Ecology, 49(4):704-726.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43162) of an [OpenML dataset](https://www.openml.org/d/43162). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43162/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43162/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43162/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

